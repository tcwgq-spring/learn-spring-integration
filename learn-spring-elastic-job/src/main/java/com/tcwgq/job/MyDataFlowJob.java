package com.tcwgq.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author iyb-wangguangqiang 2019/8/1 19:05
 */
@Slf4j
public class MyDataFlowJob implements DataflowJob<Integer> {
    @Override
    public List<Integer> fetchData(ShardingContext shardingContext) {
        return Lists.newArrayList(1, 2, 3);
    }

    @Override
    public void processData(ShardingContext shardingContext, List<Integer> data) {
        log.info(data.toString());
    }
}
