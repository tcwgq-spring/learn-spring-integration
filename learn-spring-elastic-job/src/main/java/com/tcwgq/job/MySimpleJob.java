package com.tcwgq.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;

/**
 * @author iyb-wangguangqiang 2019/8/1 19:04
 */
@Slf4j
public class MySimpleJob implements SimpleJob {
    @Override
    public void execute(ShardingContext shardingContext) {
        log.info("mySimpleJob job jobParameter {}", shardingContext.getJobParameter());
        log.info("mySimpleJob job shardingItem {} shardingParameter {}", shardingContext.getShardingItem(), shardingContext.getShardingParameter());
        log.info(DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
    }
}
