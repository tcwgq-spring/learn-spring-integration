package com.tcwgq.service;

import com.tcwgq.domain.User;

import java.util.List;

public interface UserService {
    void save(User user);

    void update(User user);

    User getUser(Integer id);

    List<User> getAllUser();

    void delete(Integer id) throws Exception;
}
