package com.tcwgq.service.impl;

import com.tcwgq.domain.User;
import com.tcwgq.service.UserService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserServiceImpl implements UserService {
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void save(User user) {
		jdbcTemplate.update("insert into t_user (name) values (?)", new Object[] { user.getName() },
				new int[] { java.sql.Types.VARCHAR });
	}

	@Override
	public void update(User user) {
		jdbcTemplate.update("update t_user set name = ? where id = ?", new Object[] { user.getName(), user.getId() },
				new int[] { java.sql.Types.VARCHAR, java.sql.Types.INTEGER });
	}

	@Override
	public User getUser(Integer id) {
		User user = (User) jdbcTemplate.queryForObject("select * from t_user where id = ?", new Object[] { id },
				new int[] { java.sql.Types.INTEGER }, new RowMapper() {
					@Override
					public Object mapRow(ResultSet rs, int index) throws SQLException {
						User user = new User(rs.getInt("id"), rs.getString("name"));
						return user;
					}
				});
		return user;
	}

	@Override
	public List<User> getAllUser() {
		@SuppressWarnings("unchecked")
		List<User> list = jdbcTemplate.query("select * from t_user", new RowMapper() {
			@Override
			public Object mapRow(ResultSet rs, int index) throws SQLException {
				return new User(rs.getInt("id"), rs.getString("name"));
			}
		});
		return list;
	}

	@Override
	public void delete(Integer id) throws Exception {
		jdbcTemplate.update("delete from t_user where id = ?", new Object[] { id },
				new int[] { java.sql.Types.INTEGER });
		jdbcTemplate.update("delete from user where id = 10");
	}

}
