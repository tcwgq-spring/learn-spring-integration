package com.tcwgq.test;

import com.tcwgq.domain.User;
import com.tcwgq.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class JdbcTest {
	ApplicationContext ctx = new ClassPathXmlApplicationContext("ApplicationContext.xml");
	UserService userService = (UserService) ctx.getBean("userService");

	@Test
	public void testSave() {
		for (int i = 1; i <= 10; i++) {
			User user = new User();
			user.setName("name_" + i);
			userService.save(user);
		}

	}

	@Test
	public void testUpdate() {
		User user = new User(1, "liSi");
		userService.update(user);
	}

	@Test
	public void testGetUser() {
		User user = userService.getUser(1);
		System.out.println(user);
	}

	@Test
	public void testGetAllUser() {
		List<User> list = userService.getAllUser();
		for (User user : list) {
			System.out.println(user);
		}
	}

	@Test
	public void testDelete() {
		try {
			userService.delete(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
