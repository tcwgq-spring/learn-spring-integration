package com.tcwgq.callback;

import org.springframework.amqp.rabbit.core.RabbitTemplate.ConfirmCallback;
import org.springframework.amqp.rabbit.support.CorrelationData;

public class MyConfirmCallback implements ConfirmCallback {

	@Override
	public void confirm(CorrelationData correlationData, boolean ack, String cause) {
		System.out.println("Exchange接收是否成功（ack）: " + ack + "。 返回的用户参数（correlationData）: " + correlationData
				+ "。NACK原因（cause） : " + cause);
	}

}