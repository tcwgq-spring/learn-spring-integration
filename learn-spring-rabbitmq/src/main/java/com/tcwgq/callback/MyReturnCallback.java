package com.tcwgq.callback;

import java.io.UnsupportedEncodingException;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate.ReturnCallback;

public class MyReturnCallback implements ReturnCallback {
	@Override
	public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
		try {
			System.out.println("消息发送进指定队列失败：失败原因（+replyText）：" + replyText + ";错误代码（replyCode）：" + replyCode + ";消息对象："
					+ new String(message.getBody(), "UTF-8") + "exchange:" + exchange + "routingKey:" + routingKey);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}
