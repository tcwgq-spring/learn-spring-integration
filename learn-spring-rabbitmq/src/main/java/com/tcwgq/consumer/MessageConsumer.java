package com.tcwgq.consumer;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;

import com.rabbitmq.client.Channel;

public class MessageConsumer implements ChannelAwareMessageListener {

	@Override
	public void onMessage(Message message, Channel channel) throws Exception {
		try {
			String srt2 = new String(message.getBody(), "UTF-8");
			System.out.println("消费者收到消息：" + srt2);
			// 成功应答
			channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
		} catch (Exception e) {
			e.printStackTrace();
			// 失败应答
			channel.basicReject(message.getMessageProperties().getDeliveryTag(), true);
		}

	}

}
