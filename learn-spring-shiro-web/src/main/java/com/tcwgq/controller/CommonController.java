package com.tcwgq.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by tcwgq on 2018/10/14 13:58.
 */
@Controller
public class CommonController {
    @RequestMapping(value = "/error", method = {RequestMethod.GET, RequestMethod.POST})
    public String error() {
        return "error";
    }

    @RequestMapping(value = "/unauth", method = {RequestMethod.GET, RequestMethod.POST})
    public String unauth() {
        return "unauth";
    }
}
