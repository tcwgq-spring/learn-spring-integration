package com.tcwgq.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 权限表 前端控制器
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
@Controller
@RequestMapping("/perm")
public class PermController {

}

