package com.tcwgq.exception;

/**
 * Created by tcwgq on 2018/10/2 13:20.
 */
public interface ErrorCode {
    public Integer getCode();

    public String getMsg();
}

