package com.tcwgq.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcwgq.entity.Perm;

import java.util.List;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
public interface PermMapper extends BaseMapper<Perm> {
    public List<Perm> findByRoleId(Integer roleId);
}
