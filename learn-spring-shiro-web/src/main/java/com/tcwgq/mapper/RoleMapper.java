package com.tcwgq.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tcwgq.entity.Role;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
public interface RoleMapper extends BaseMapper<Role> {

    public List<Role> findByUserId(Integer userId);

}
