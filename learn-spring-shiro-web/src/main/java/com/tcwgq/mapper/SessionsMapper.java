package com.tcwgq.mapper;

import com.tcwgq.entity.Sessions;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 16:39:29
 */
public interface SessionsMapper extends BaseMapper<Sessions> {

}
