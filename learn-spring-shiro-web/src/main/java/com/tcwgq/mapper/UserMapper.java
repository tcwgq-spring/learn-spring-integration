package com.tcwgq.mapper;

import com.tcwgq.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
public interface UserMapper extends BaseMapper<User> {

    public User getByUsername(@Param("username") String username);

}
