package com.tcwgq.realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author wangguangqiang
 * @Date 2018年9月7日 下午3:13:59
 * @Email wangguangqiang@jd.com
 */
public class UserRealm extends AuthorizingRealm {

	/**
	 * 授权
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		String userName = (String) principals.getPrimaryPrincipal();
		List<String> permissionList = new ArrayList<String>();
		permissionList.add("user:add");
		permissionList.add("user:delete");
		if (userName.equals("zhou")) {
			permissionList.add("user:query");
		}
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		info.addStringPermissions(permissionList);
		info.addRole("admin");
		return info;
	}

	/**
	 * 认证
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		String userName = (String) token.getPrincipal();
		if ("".equals(userName)) {
			return null;
		}
		SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(userName, "123456", this.getName());
		return info;
	}

}
