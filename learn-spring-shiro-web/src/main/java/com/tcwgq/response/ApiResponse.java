package com.tcwgq.response;

import com.tcwgq.exception.ErrorCode;

import java.io.Serializable;

/**
 * Created by tcwgq on 2018/10/2 13:40.
 */
public class ApiResponse<T> implements Serializable {
    private Integer code;
    private String msg;
    private T data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <T> ApiResponse<T> success(T data) {
        ApiResponse<T> response = new ApiResponse<>();
        response.code = 0;
        response.msg = "OK";
        response.data = data;
        return response;
    }

    public static <T> ApiResponse<T> success(int code, T data) {
        ApiResponse<T> response = new ApiResponse<>();
        response.code = code;
        response.msg = "OK";
        response.data = data;
        return response;
    }

    public static <T> ApiResponse<T> success(int code, String msg, T data) {
        ApiResponse<T> response = new ApiResponse<>();
        response.code = code;
        response.msg = msg;
        response.data = data;
        return response;
    }

    public static <T> ApiResponse<T> fail(ErrorCode errorCode) {
        ApiResponse<T> response = new ApiResponse<>();
        response.code = errorCode.getCode();
        response.msg = errorCode.getMsg();
        return response;
    }

    public static <T> ApiResponse<T> fail(int code, String msg) {
        ApiResponse<T> response = new ApiResponse<>();
        response.code = code;
        response.msg = msg;
        return response;
    }

    /**
     * 特殊情况下使用
     *
     * @param code
     * @param msg
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ApiResponse<T> fail(int code, String msg, T data) {
        ApiResponse<T> response = new ApiResponse<>();
        response.code = code;
        response.msg = msg;
        return response;
    }
}
