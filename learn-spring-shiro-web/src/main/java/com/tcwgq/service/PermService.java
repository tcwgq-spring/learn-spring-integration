package com.tcwgq.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcwgq.entity.Perm;

import java.util.List;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
public interface PermService extends IService<Perm> {

    public List<Perm> findByRoleId(Integer roleId);

}
