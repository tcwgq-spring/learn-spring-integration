package com.tcwgq.service;

import com.baomidou.mybatisplus.service.IService;
import com.tcwgq.entity.Role;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
public interface RoleService extends IService<Role> {
    public List<Role> findByUserId(Integer userId);

}
