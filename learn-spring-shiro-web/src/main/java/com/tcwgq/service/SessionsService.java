package com.tcwgq.service;

import com.tcwgq.entity.Sessions;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 16:39:29
 */
public interface SessionsService extends IService<Sessions> {

}
