package com.tcwgq.service.impl;

import com.tcwgq.entity.Sessions;
import com.tcwgq.mapper.SessionsMapper;
import com.tcwgq.service.SessionsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 16:39:29
 */
@Service
public class SessionsServiceImpl extends ServiceImpl<SessionsMapper, Sessions> implements SessionsService {

}
