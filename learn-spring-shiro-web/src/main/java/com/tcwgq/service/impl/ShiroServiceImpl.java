package com.tcwgq.service.impl;

import com.tcwgq.service.ShiroService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;

/**
 * Created by tcwgq on 2018/10/14 10:43.
 */
@Service
public class ShiroServiceImpl implements ShiroService {
    @Override
    public void doLogin(String username, String password) {
        Subject subject = SecurityUtils.getSubject();
        if (!subject.isAuthenticated()) {
            UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, password);
            usernamePasswordToken.setRememberMe(true);
            subject.login(usernamePasswordToken);
        }
    }
}
