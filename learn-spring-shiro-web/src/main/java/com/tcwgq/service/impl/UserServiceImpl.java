package com.tcwgq.service.impl;

import com.tcwgq.entity.User;
import com.tcwgq.mapper.UserMapper;
import com.tcwgq.service.UserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-14 10:40:52
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUser(String username) {
        return userMapper.getByUsername(username);
    }
}
