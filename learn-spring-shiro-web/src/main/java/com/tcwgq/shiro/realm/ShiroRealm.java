package com.tcwgq.shiro.realm;

import com.tcwgq.entity.Perm;
import com.tcwgq.entity.Role;
import com.tcwgq.entity.User;
import com.tcwgq.service.PermService;
import com.tcwgq.service.RoleService;
import com.tcwgq.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by tcwgq on 2018/10/10 22:43.
 */
public class ShiroRealm extends AuthorizingRealm {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private PermService permService;

    /**
     * 获取认证信息
     *
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
        String username = usernamePasswordToken.getUsername();
        if (StringUtils.isNotBlank(username)) {
            User user = userService.getUser(username);
            if (user != null) {
                return new SimpleAuthenticationInfo(username, user.getPassword(), getName());
            }
        }
        return null;
    }

    /**
     * 获取授权信息
     *
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = (String) principals.fromRealm(getName()).iterator().next();
        if (username != null) {
            SimpleAuthorizationInfo simpleAuthenticationInfo = new SimpleAuthorizationInfo();
            User user = userService.getUser(username);
            if (user != null) {
                // 设置角色
                List<Role> roles = roleService.findByUserId(user.getId());
                for (Role role : roles) {
                    simpleAuthenticationInfo.addRole(role.getName());
                    List<Perm> perms = permService.findByRoleId(role.getId());
                    for (Perm perm : perms) {
                        simpleAuthenticationInfo.addStringPermission(perm.getUrl());
                    }
                }
                return simpleAuthenticationInfo;
            }

        }
        return null;
    }

}
