package com.tcwgq.shiro.session;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.session.mgt.SessionFactory;
import org.apache.shiro.session.mgt.SimpleSession;
import org.apache.shiro.web.session.mgt.WebSessionContext;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by tcwgq on 2018/10/14 15:14.
 */
public class SysSessionFactory implements SessionFactory {
    @Override
    public Session createSession(SessionContext initData) {
        UserSession session = new UserSession();
        if (initData != null && initData instanceof WebSessionContext) {
            WebSessionContext sessionContext = (WebSessionContext) initData;
            HttpServletRequest request = (HttpServletRequest) sessionContext.getServletRequest();
            if (request != null) {
                session.setHost(request.getRemoteHost());
                session.setUserAgent(request.getHeader("User-Agent"));
            }
        }
        return session;
    }
}
