package com.tcwgq.shiro.session;

import org.apache.shiro.session.mgt.SimpleSession;

/**
 * Created by tcwgq on 2018/10/14 16:29.
 */
public class UserSession extends SimpleSession {
    private static final long serialVersionUID = 1L;

    public static enum OnlineStatus {
        /**
         * 在线
         */
        ON_LINE("在线"),

        /**
         * 离线
         */
        OFF_LINE("离线"),

        /**
         * 强制退出
         */
        FORCE_LOGOUT("强制退出");

        private final String info;

        private OnlineStatus(String info) {
            this.info = info;
        }

        public String getInfo() {
            return info;
        }
    }

    // 用户浏览器类型
    private String userAgent;

    // 在线状态
    private OnlineStatus status = OnlineStatus.OFF_LINE;

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public OnlineStatus getStatus() {
        return status;
    }

    public void setStatus(OnlineStatus status) {
        this.status = status;
    }
}
