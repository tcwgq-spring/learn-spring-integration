package com.tcwgq.utils;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by tcwgq on 2018/10/14 18:18.
 */
public class ShiroUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShiroUtil.class);

    /**
     * 是否拥有该权限
     *
     * @param permission 权限标识
     * @return true：是 false：否
     */
    public static boolean hasPermission(String permission) {
        LOGGER.info(permission);
        Subject subject = SecurityUtils.getSubject();
        return subject != null && subject.isPermitted(permission);
    }

    /**
     * 是否拥有该权限
     *
     * @param permission 权限标识
     * @return true：是 false：否
     */
    public static boolean hasPermissionInMethod(String permission) {
        Subject subject = SecurityUtils.getSubject();
        return subject != null && subject.isPermitted(permission);
    }

    public static Session getSession() {
        return SecurityUtils.getSubject().getSession();
    }

    public static Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    public static String getUser() {
        return (String) SecurityUtils.getSubject().getPrincipal();
    }

    public static void setSessionAttribute(Object key, Object value) {
        getSession().setAttribute(key, value);
    }

    public static Object getSessionAttribute(Object key) {
        return getSession().getAttribute(key);
    }

    public static boolean isLogin() {
        return SecurityUtils.getSubject().getPrincipal() != null;
    }

    public static void logout() {
        SecurityUtils.getSubject().logout();
    }

    public static String getKaptcha(String key) {
        String kaptcha = getSessionAttribute(key).toString();
        getSession().removeAttribute(key);
        return kaptcha;
    }

}
